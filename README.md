# Installation (needs [Docker](https://www.docker.com/get-docker))

1. Pull image: `docker pull fillpdfservice/pdfapi`
2. Run image, forwarding the port for the API if necessary (not necessary if
being accessed from another container):
`docker run -p0.0.0.0:8085:8080 fillpdfservice/pdfapi ` (you can also use a Docker
Compose file).

# API documentation

Paths are relative to the URL to the server. The default port is `8080`.

## `POST /api/v1/parse`

Pass a JSON object containing a `pdf` key and the PDF encoded in Base64.

Sample request:

```
# In the real world, you would be unlikely to use cURL. This is simply a
# platform-neutral example.
# The pipe to `python` is just for formatting.

curl --header "Content-Type: application/json"\
--data '{"pdf": "'$(base64 src/test/resources/fillpdf_test_v4.pdf)'"}'
http://localhost:8085/api/v1/parse | python -m json.tool
######################################################################## 100.0%
[
    {
        "name": "List Box6",
        "type": "List",
        "value": "ListItem3"
    },
    {
        "name": "List Box7",
        "type": "List",
        "value": ""
    },
    {
        "name": "Radio Button3",
        "type": "Radiobutton",
        "value": "Choice 1"
    },
    {
        "name": "Signature9",
        "type": "Signature",
        "value": ""
    },
    {
        "name": "Radio Button4",
        "type": "Radiobutton",
        "value": "Other Choice 1"
    },
    {
        "name": "Text2",
        "type": "Text",
        "value": ""
    },
    {
        "name": "Text1",
        "type": "Text",
        "value": ""
    },
    {
        "name": "Combo Box8",
        "type": "Combobox",
        "value": "Combo1"
    },
    {
        "name": "Button2",
        "type": "Pushbutton",
        "value": ""
    },
    {
        "name": "Check Box5",
        "type": "Checkbox",
        "value": ""
    },
    {
        "name": "Check Box4",
        "type": "Checkbox",
        "value": ""
    }
]
```

## `POST /api/v1/merge`

Pass a JSON object containing a `pdf` key and a `fields` key.

The `pdf` key is the same as in `/api/v1/parse`.

The `fields` key
should be an object:

- whose keys correspond to the field names returned by
`/api/v1/parse`.
- whose values are objects containing `type` keys of `"text"`
or `"image"` and `data` keys. If the `type` is `"text"`, then `data` should
contain the string. If the `type` is `"image"`, then it should contain the
image encoded in Base64.

You can optionally pass a `flatten` key with a value of `true` or `false`. If
you pass it as `false`, then the fields will be left editable after being
filled in.

See the example.

```
echo '{
  "pdf": "'$(base64 src/test/resources/fillpdf_test_v4.pdf)'",
  "fields": {
    "Text1": { "type": "text", "data": "Test text 1" },
    "Button2": { "type": "image", "data": "'$(base64 src/test/resources/testphoto.jpg)'" } 
  }
}' > /tmp/merge.json

curl --header "Content-Type: application/json" --data @/tmp/merge.json http://localhost:8085/api/v1/merge | python -m json.tool

[output truncated; too big]
```

The return value will be a JSON array with a single `pdf` key containing a
Base64 representation of the PDF with the fields filled in, flattened or not
depending on the value of `flatten` in the request.

# Running from source

Build and run the JAR (e.g. `java -jar out/artifacts/pdfapi/pdfapi.jar`). It contains an embedded Tomcat 8.5.x server.

# Building with Docker

First, build the project in IntelliJ IDEA so that the JAR exists.

`docker build . --build-arg JAR_FILE=/path/to/pdfapi.jar -t fillpdfservice/pdfapi`

# Sponsoring development

Does FillPDF LocalServer help you solve your problems or make money? I'd really appreciate you contributing towards its maintenance and development.

Patreon (recurring): <a href="https://www.patreon.com/bePatron?u=357298" data-patreon-widget-type="become-patron-button"><img title="Sponsor FillPDF LocalServer!" src="https://c5.patreon.com/external/logo/become_a_patron_button@2x.png" /></a>

Ko-Fi (one-time): [![ko-fi](https://www.ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/H2H71LLJP)
