package io.fillpdf.pdfapi;

import com.lowagie.text.DocumentException;
import com.ocdevel.FillpdfService;
import org.apache.commons.codec.binary.Base64;

import java.io.IOException;

public class FieldMappingProcessor extends Visitor {

    private FillpdfService service;
    private String key;

    FieldMappingProcessor(FillpdfService service, String key) {
        this.service = service;
        this.key = key;
    }

    @Override
    public void visit(TextFieldMapping mapping) throws IOException, DocumentException {
        this.service.text(this.key, mapping.getData());
    }

    @Override
    public void visit(ImageFieldMapping mapping) throws IOException, DocumentException {
        this.service.image(this.key, Base64.decodeBase64(mapping.getData()));
    }

}
